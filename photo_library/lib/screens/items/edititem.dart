import 'package:flutter/material.dart';
import '../../models/framework.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';

class EditItemPage extends StatefulWidget {
  final Item item;
  final Album album;
  EditItemPage({Key key, this.item, this.album}) : super(key: key);
  @override
  createState() => _EditItemPageState();
}

class _EditItemPageState  extends State<EditItemPage> {
  int refresh = 0;
  List<String> _newTags;
  bool _changeImage = false;
  String _newItemDescription;
  File _image;
  List<String> _finalTags;

  removeTag(String tag) {
    setState(() {
      refresh = 1;
    });
    widget.item.itemTags.remove(tag);
  }

  addTag(String tag) {
    print("tag");
    setState(() {
      _newTags.add(tag);
      refresh = 1;
    });
    print(_newTags);
  }

  //function for getting image from camera or gallery
  Future getImage(bool isCamera) async {
    File image;
    if (isCamera) {
      image = await ImagePicker.pickImage(source: ImageSource.camera);
    } else {
      image = await ImagePicker.pickImage(source: ImageSource.gallery);
    }
    // Directory appDocDir = await getApplicationDocumentsDirectory();
    // String appDocPath = appDocDir.path;
    // File newImage = await image.copy('$appDocPath/library/image1.png');
    setState(() {
      _image = image;
    });
  }

  editItem(BuildContext context, Function func) {
    Navigator.of(context).pop(true);
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text("Item Edited"),
          content: Text("New Item's Description is $_newItemDescription"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
          ],
        );
      },
    );
    func();
  }

  _showDialog() {
    _newTags = List<String>();
    final TextEditingController _tagController = new TextEditingController();
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Column(
            children: <Widget>[
              Text(
                'Please Select tag or Give your tag',
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 10.0,
              ),
              TextField(
                  controller: _tagController,
                  autofocus: false,
                  autocorrect: true,
                  textAlign: TextAlign.center,
                  onSubmitted: (value) {
                    addTag(value);
                    _tagController.clear();
                  },
                  decoration: InputDecoration(
                    hintText: "Enter Tag Here",
                    hintStyle: TextStyle(
                        fontWeight: FontWeight.w300, color: Colors.red),
                    border: OutlineInputBorder(),
                  )),
            ],
          ),
          content: SingleChildScrollView(
            child: Container(
              child: Column(
                children: <Widget>[
                  SizedBox(height: 10.0),
                  ListTile(
                    title: Text('Birthday'),
                    onTap: () {
                      addTag('Wedding');
                    },
                  ),
                  ListTile(
                    title: Text('Graduation'),
                    onTap: () {
                      addTag('Graduation');
                    },
                  ),
                  ListTile(
                    title: Text('Road Trip'),
                    onTap: () {
                      addTag('Road Trip');
                    },
                  ),
                  ListTile(
                    title: Text('Party'),
                    onTap: () {
                      addTag('Party');
                    },
                  ),
                  ListTile(
                    title: Text('Hiking'),
                    onTap: () {
                      addTag('Hiking');
                    },
                  ),
                  ListTile(
                    title: Text('Swimming'),
                    onTap: () {
                      addTag('Swimming');
                    },
                  ),
                  ListTile(
                    title: Text('Dinner'),
                    onTap: () {
                      addTag('Dinner');
                    },
                  ),
                  ListTile(
                    title: Text('Riding'),
                    onTap: () {
                      addTag('Riding');
                    },
                  ),
                  ListTile(
                    title: Text('Camping'),
                    onTap: () {
                      addTag('Camping');
                    },
                  ),
                ],
              ),
            ),
          ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            Row(
              children: <Widget>[
                FlatButton(
                  child: Text("Done"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text("Close"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            )
          ],
        );
      },
    );
  }

  removeThisItem(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text("Warning"),
          content: Text("Do you want to remove this item?"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text("Yes"),
              onPressed: () {
                widget.album.items.remove(widget.item);
                Navigator.of(context).pushNamed('/library');
              },
            ),
            FlatButton(
              child: Text("No"),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Item'),
      ),
      body: ListView(
        padding: const EdgeInsets.all(20.0),
        children: <Widget>[
          Text(
            widget.item.itemDescr,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 10),
          TextField(
            autocorrect: true,
            autofocus: false,
            textAlign: TextAlign.center,
            decoration: InputDecoration(
              hintText: 'Please Type New Item Description',
            ),
            onChanged: (value) {
              setState(() {
                _newItemDescription = value;
              });
            },
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              _image == null
                  ? Image.file(widget.item.itemImage,
                      height: 150.0, width: 150.0)
                  : Image.file(_image, height: 150.0, width: 150.0),
              _changeImage
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        RaisedButton(
                          child: Text(
                            'Choose from Gallery',
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Colors.blue,
                          onPressed: () {
                            getImage(false);
                          },
                        ),
                        RaisedButton(
                          child: Text(
                            'Choose from Camera',
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Colors.blue,
                          onPressed: () {
                            getImage(true);
                          },
                        )
                      ],
                    )
                  : RaisedButton(
                      child: Text(
                        'Change Image',
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        setState(() {
                          _changeImage = true;
                        });
                      },
                      color: Colors.blue,
                    )
            ],
          ),
          SizedBox(
            height: 30.0,
          ),
          Text('Old Tags:'),
          Wrap(
              children: widget.item.itemTags != null &&
                      widget.item.itemTags.length != 0
                  ? widget.item.itemTags
                      .map((itemTag) => Stack(
                            children: <Widget>[
                              FilterChip(
                                label: Text(itemTag),
                                onSelected: (bool value) {
                                  print("selected");
                                },
                              ),
                              InkWell(
                                child: Icon(Icons.cancel),
                                onTap: () {
                                  removeTag(itemTag);
                                },
                              )
                            ],
                          ))
                      .toList()
                  : [Text('no Tags')]),
          Text('New Tags:'),
          Wrap(
              children: _newTags != null && _newTags.length != 0
                  ? _newTags
                      .map((itemTag) => Stack(
                            children: <Widget>[
                              FilterChip(
                                label: Text(itemTag),
                                onSelected: (bool value) {
                                  print("selected");
                                },
                              ),
                              InkWell(
                                child: Icon(Icons.cancel),
                                onTap: () {
                                  _newTags.remove(itemTag);
                                  setState(() {
                                    refresh = 1;
                                  });
                                },
                              )
                            ],
                          ))
                      .toList()
                  : [Text('New Tag is empty')]),
          FloatingActionButton(
            child: Icon(Icons.textsms),
            onPressed: () {
              _showDialog();
            },
          ),
          SizedBox(
            height: 50.0,
          ),
          RaisedButton(
            child: Text(
              'Confirm',
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () {
              if(_newTags!=null&& widget.item.itemTags==null){
                _finalTags = _newTags;
              }else if(_newTags==null && widget.item.itemTags==null){
                _finalTags=[''];
              }else if(_newTags==null && widget.item.itemTags!=null){
                _finalTags=widget.item.itemTags;
              }else if(_newTags!=null && widget.item.itemTags!=null){
                _finalTags = List.from(widget.item.itemTags)..addAll(_newTags);
              }
              print(_finalTags);
              print('hello');
              if (_image != null &&
                  _newItemDescription == null) {
                  print('image xa name xaina');
                editItem(
                    context,
                    widget.item
                        .editItem(widget.item.itemDescr, _image, _finalTags));
              } else if (_image == null && _newItemDescription != null) {
                print('image xaina name xa');
                editItem(
                    context,
                    widget.item.editItem(_newItemDescription,
                        widget.item.itemImage, _finalTags));
              } else if (_image != null && _newItemDescription != null) {
                print('image xa name xa');
                editItem(
                    context,
                    widget.item
                        .editItem(_newItemDescription, _image, _finalTags));
              }else{
                showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          // return object of type Dialog
                          return AlertDialog(
                            title: Text("Warning"),
                            content: Text('Please change something'),
                            actions: <Widget>[
                              // usually buttons at the bottom of the dialog
                              FlatButton(
                                child: Text("Close"),
                                onPressed: () {
                                  Navigator.of(context).pop(true);
                                },
                              ),
                            ],
                          );
                        },
                      );
              }
            },
            color: Colors.blue,
          ),
          RaisedButton(
            onPressed: () {
              removeThisItem(context);
            },
            child: Text(
              'Remove this Item',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.red,
          ),
        ],
      ),
    );
  }
}
