import 'package:flutter/material.dart';
import '../../models/framework.dart';
import './additem.dart';
import './edititem.dart';

class ItemsPage extends StatefulWidget {
  final Album album;
  ItemsPage({Key key,this.album}) : super(key: key);
  @override 
  createState() =>_ItemsPageState();
}

class _ItemsPageState extends State<ItemsPage>{
  int refresh=0;

  addItemPage(BuildContext context, Album album) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => AddItemPage(album: album),
      ),
    );
    setState(() {
     refresh=1; 
    });
  }

  editItemPage(BuildContext context,Item item,Album album){
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EditItemPage(album:album,item: item),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.album.albumName),
      ),
      body: ListView(
        padding: EdgeInsets.all(20.0),
        children: <Widget>[
          Wrap(
            alignment: WrapAlignment.spaceEvenly,
            runSpacing: 40.0,
            children: widget.album.items != null
                ? widget.album.items
                    .map((item) => GestureDetector(
                        onDoubleTap: () {editItemPage(context,item,widget.album);},
                        child: Column(
                          children: <Widget>[
                            Image.file(
                              item.itemImage,
                              height: 150.0,
                              width: 150.0,
                            ),
                          ],
                        )))
                    .toList()
                : Text('Album is Empty'),
          ),
          SizedBox(
            height: 15.0,
          ),
          FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () {
              addItemPage(context, widget.album);
            },
          ),
        ],
      ),
    );
  }
}

