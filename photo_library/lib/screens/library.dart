import 'dart:io';
import 'package:flutter/material.dart';
import 'package:photo_library/screens/album/editalbum.dart';
import 'package:photo_library/screens/items/items.dart';
import 'package:scoped_model/scoped_model.dart';
import '../models/framework.dart';
import './items/listbytagitems.dart';
import 'package:responsive_container/responsive_container.dart';

class LibraryPage extends StatefulWidget {
  @override
  createState() => _LibraryPageState();
}

class _LibraryPageState extends State<LibraryPage> {
  //for navigating to add-album page
  void addAlbumPage(BuildContext context) {
    Navigator.of(context).pushNamed('/addalbum');
  }

  //for navigating to items page
  void itemPage(BuildContext context, Album album) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ItemsPage(album: album),
      ),
    );
  }

  //for navigating to edit album page
  void editAlbumPage(BuildContext context, Album album) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EditAlbumPage(album: album),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<Library>(
        builder: (context, child, model) => WillPopScope(
              child: Scaffold(
                appBar: AppBar(
                  leading: Container(),
                  title: Text('${model.libraryName}'),
                  actions: <Widget>[
                    IconButton(
                      icon: Icon(Icons.search),
                      onPressed: () {
                        showSearch(
                          context: context,
                          delegate: TagSearch(model),
                        );
                      },
                    ),
                    IconButton(
                      icon: Icon(Icons.exit_to_app),
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            // return object of type Dialog
                            return AlertDialog(
                              title: Text("Warning!!"),
                              content: Text("Do you want to logout?"),
                              actions: <Widget>[
                                // usually buttons at the bottom of the dialog
                                FlatButton(
                                  child: Text("Yes"),
                                  onPressed: () {
                                    Navigator.of(context)
                                        .pushNamedAndRemoveUntil('/login',
                                            (Route<dynamic> route) => false);
                                  },
                                ),
                                FlatButton(
                                  child: Text("No"),
                                  onPressed: () {
                                    Navigator.of(context).pop(true);
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      },
                    ),
                  ],
                ),
                body: ListView(
                  padding: EdgeInsets.all(20.0),
                  children: <Widget>[
                    RaisedButton(
                        child: Text('See Modified Albums'),
                        onPressed: () {
                          model.getModifiedAlbum();
                        }),
                    RaisedButton(
                        child: Text('Write File'),
                        onPressed: () {
                          String library1Json = model.toJson().toString();
                          model.writeSeedFile(library1Json);
                          print('File is written');
                        }),
                    Wrap(
                      alignment: WrapAlignment.center,
                      spacing: 10.0,
                      runSpacing: 20.0,
                      children: model.albums.length != 0
                          ? model.albums
                              .map((album) => ExpansionTile(
                                    key: Key(album.albumName),
                                    title: album.albumIcon is File
                                        ? Image.file(
                                            album.albumIcon,
                                            height: 150.0,
                                            width: 150.0,
                                          )
                                        : Text(album.albumIcon),
                                    leading: Text(album.albumName),
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          RaisedButton(
                                            child: Text('View Album'),
                                            color: Colors.blue,
                                            textColor: Colors.white,
                                            onPressed: () {
                                              itemPage(context, album);
                                            },
                                          ),
                                          RaisedButton(
                                            child: Text('Edit Album'),
                                            color: Colors.blue,
                                            textColor: Colors.white,
                                            onPressed: () {
                                              editAlbumPage(context, album);
                                            },
                                          )
                                        ],
                                      ),
                                    ],
                                  ))
                              .toList()
                          : [
                              Column(
                                children: <Widget>[
                                  SizedBox(height: 30.0),
                                  Text('Library is Empty'),
                                  SizedBox(height: 15.0),
                                  Text(
                                      'Please press below button for adding new album'),
                                  SizedBox(height: 15.0),
                                  Icon(Icons.arrow_downward),
                                ],
                              )
                            ],
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    FloatingActionButton(
                      child: Icon(Icons.add),
                      onPressed: () {
                        addAlbumPage(context);
                      },
                    ),
                  ],
                ),
              ),
              onWillPop: () async => false,
            ));
  }
}

class TagSearch extends SearchDelegate<Library> {
  final Library mylib;
  TagSearch(this.mylib);
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  showItems(BuildContext context, List<Item> items, String tag) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ListByTagItems(items: items, tag: tag),
      ),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return ScopedModelDescendant<Library>(
      builder: (context, child, model) => Column(children: <Widget>[
            Text('Here is the list of album containing $query'),
            SizedBox(height: 20.0),
            ResponsiveContainer(
              heightPercent: 80,
              widthPercent: 100,
              child: ListView(
                  children: mylib.albums != null && mylib.albums.length != 0
                      ? mylib.albums
                          .map((album) => Column(
                                children: <Widget>[
                                  album.getItemByTagAlbum(query) == true
                                      ? GestureDetector(
                                          onTap: () {
                                            showItems(
                                                context, album.items, query);
                                          },
                                          child: album.albumIcon is File
                                              ? Image.file(
                                                  album.albumIcon,
                                                  height: 200.0,
                                                  width: 200.0,
                                                )
                                              : Container(
                                                  height: 200,
                                                  width: 200,
                                                  child: Align(
                                                    alignment: Alignment.center,
                                                    child: Text(
                                                        'Album Icon not found'),
                                                  ),
                                                ),
                                        )
                                      : Text(
                                          "Album '${album.albumName}' doesnot contain $query"),
                                  SizedBox(height: 20.0)
                                ],
                              ))
                          .toList()
                      : [Text('nothing here')]),
            )
          ]),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Container(
      child: Text(query),
    );
  }
}
